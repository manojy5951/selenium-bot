import time
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
url = 'https://www.google.com/gmail/'
urllogin = 'https://accounts.google.com/AccountChooser?service=mail&continue=https://mail.google.com/mail/'
chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('disable-infobars')

browser = webdriver.Chrome(executable_path='path to webdriver executable',chrome_options=chrome_options)
browser.get('http://www.google.com')
inps=browser.find_element_by_name('q')
inps.send_keys("gmail")
btn = browser.find_element(By.NAME,'btnK')
btn.click()

gmail_link = browser.find_element_by_xpath('//a[@href="'+url+'"]')
gmail_link.click()

gmail_ac_login_link = browser.find_element_by_xpath('//a[@href="'+urllogin+'"]')
gmail_ac_login_link.click()


gmail_userId_text = browser.find_element_by_id('identifierId')
gmail_userId_text.send_keys('userid')
gmail_userId_text.send_keys(u'\ue007')
time.sleep(3)
gmail_passwd_text = browser.find_element_by_class_name('whsOnd')
gmail_passwd_text.click()
gmail_passwd_text.send_keys('passwrd')
gmail_passwd_text.send_keys(u'\ue007')
time.sleep(3)

compose_email_btn = browser.find_element_by_id(':i0')
compose_email_btn.click()

profile_button = browser.find_element_by_class_name('gb_9a').click()
signout_btn = browser.find_element_by_id('gb_71').click()

browser.close()

